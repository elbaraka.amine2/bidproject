package com.example.bidproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BidprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(BidprojectApplication.class, args);
	}

}
